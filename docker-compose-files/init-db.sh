#!/bin/bash
set -e

psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "$POSTGRES_DB" <<-EOSQL
    CREATE USER ${DG_KEYCLOAK_DB_USER} WITH PASSWORD '${DG_KEYCLOAK_DB_PASSWORD}';
    CREATE DATABASE ${DG_KEYCLOAK_DB_NAME} OWNER ${DG_KEYCLOAK_DB_USER};
    GRANT ALL PRIVILEGES ON DATABASE ${DG_KEYCLOAK_DB_NAME} TO ${DG_KEYCLOAK_DB_USER};
EOSQL