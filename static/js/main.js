$( document ).ready(function() {
    $('[data-toggle="tooltip"]').tooltip({
        'delay': {show: 500, hide: 100}
    });
});
const DEBUG = true
function log(text, obj) {
    if (!DEBUG) { return }
    const currentdate = new Date();
    const datetime = currentdate.getDate() + "."
                    + (currentdate.getMonth()+1)  + "."
                    + currentdate.getFullYear() + " "
                    + currentdate.getHours() + ":"
                    + currentdate.getMinutes() + ":"
                    + currentdate.getSeconds() + ":"
                    + currentdate.getMilliseconds();
    console.log(datetime, text, obj);
}

// function setCookie(name,value,days) {
//     let expires = "";
//     if (days) {
//         let date = new Date();
//         date.setTime(date.getTime() + (days*24*60*60*1000));
//         expires = "; expires=" + date.toUTCString();
//     }
//     document.cookie = name + "=" + (value || "")  + expires + "; path=/";
// }
function getCookie(name) {
    let nameEQ = name + "=";
    let ca = document.cookie.split(';');
    for(let i=0;i < ca.length;i++) {
        let c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1,c.length);
        if (c.indexOf(nameEQ) === 0) return c.substring(nameEQ.length,c.length);
    }
    return null;
}

function csrfSafeMethod(method) {
    // these HTTP methods do not require CSRF protection
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}

$.ajaxSetup({
 beforeSend: function(xhr, settings) {
     if (!csrfSafeMethod(settings.type) && !this.crossDomain &&
        (!(/^http:.*/.test(settings.url) || /^https:.*/.test(settings.url))) ) {
         // Only send the token to relative URLs i.e. locally.
         xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
     }
 }
});

// function sleep(delay) {
//     var start = new Date().getTime();
//     while (new Date().getTime() < start + delay);
// }

function psleep(ms) {
  return new Promise(resolve => {
    setTimeout(resolve, ms);
  });
}