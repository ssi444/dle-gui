from django.urls import path
from . import views

urlpatterns = [
    path('', views.show_clones),
    path('create', views.clone_create),
    path('status', views.clone_status),
    path('action', views.clone_action),
    path('reset', views.clone_reset_with_new_snapshot),
]
