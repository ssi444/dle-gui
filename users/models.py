from django.db import models
from django.contrib.auth.models import User, Group


class CloneActions(models.Model):
    """Действия с клонами"""
    action = models.CharField("Идентификатор действия", max_length=20, unique=True)
    description = models.CharField("Описание действия", max_length=50)

    def __str__(self):
        return f"{self.action} ({self.description})"

    class Meta:
        verbose_name = "Действие с клоном"
        verbose_name_plural = "Действия с клонами"


class ActionLog(models.Model):
    """Класс для логирования действий пользователей"""
    # datatime = models.DateTimeField("ДатаВремя", default=datetime.datetime.now)
    datatime = models.DateTimeField("ДатаВремя", auto_now=True)
    user = models.ForeignKey(User, verbose_name="Пользователь", null=False, on_delete=models.DO_NOTHING)
    action = models.ForeignKey(CloneActions, verbose_name="Действие", null=False, on_delete=models.PROTECT)
    success = models.BooleanField("Успешное действие", null=False, default=False)
    clone = models.CharField("Идентификатор клона", max_length=250, null=False, default='-')
    description = models.CharField("Идентификатор клона", max_length=2000, null=True, default='')

    def __str__(self):
        return f"{self.datatime} | user={self.user} | action={self.action} | success={self.success} | clone={self.clone} | description={self.description}"

    class Meta:
        verbose_name = "Лог действий"
        verbose_name_plural = "Логи действий"


class Permissions(models.Model):
    """Привилегии"""
    permission = models.CharField("Идентификатор привилегии", max_length=50, unique=True)
    description = models.CharField("Описание привилегии", max_length=100)
    group = models.CharField("Группа привилегий", max_length=50, default="other")
    priority = models.IntegerField("Приоритет привилегии в группе", default=0)

    def __str__(self):
        return f"{self.permission} \t ({self.description}) \t | group={self.group} \t | priority={self.priority}"

    class Meta:
        verbose_name = "Привилегия"
        verbose_name_plural = "Привилегии"


class Roles(models.Model):
    """Роли"""
    role = models.CharField("Идентификатор роли", max_length=50, unique=True)
    description = models.CharField("Описание роли", max_length=100)
    permission = models.ManyToManyField(Permissions, verbose_name="Привилегия")

    def __str__(self):
        return f"{self.role} ({self.description})"

    class Meta:
        verbose_name = "Роль"
        verbose_name_plural = "Роли"


class Group(Group):
    role = models.ForeignKey(Roles, verbose_name="Роль", null=True, on_delete=models.DO_NOTHING)
    user = models.ManyToManyField(User, verbose_name="Пользователь", blank=True)

    class Meta:
        # proxy = True
        verbose_name = "Группа"
        verbose_name_plural = "Группы"
