from django.contrib import admin
from .models import CloneActions, ActionLog, Permissions, Roles, Group

admin.site.register(CloneActions)
admin.site.register(ActionLog)
admin.site.register(Permissions)
admin.site.register(Roles)
admin.site.register(Group)
