from django.http import JsonResponse
from DLE_gui import settings
import requests
import time


def health_check(request):
    start_time = time.time()
    r = requests.get(f"{settings.DBLAB['url']}/healthz")
    if r.status_code != 200:
        return JsonResponse({"version": settings.VERSION, "status": "ERROR", "msg": "DLE not available",
                             "response_time": round(time.time() - start_time, 3)}, status=540)
    else:
        return JsonResponse({"version": settings.VERSION, "status": "OK", "msg": "OK",
                             "response_time": round(time.time() - start_time, 3)}, status=200)
