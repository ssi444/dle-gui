#!/bin/bash

function start {
  python manage.py runserver 0.0.0.0:8000
}

function migrate {
  python manage.py migrate
}


function load_initial_data {
  find initial-data/ -type f | sort -n | xargs -n 1 -I% sh -c 'echo "Loading initial data from file: %" && python manage.py loaddata %'
}


function create_superuser {
username="admin"
email="admin@domain.local"
password="admin"

if [[ -n $DG_ADMIN_USERNAME ]]; then username=$DG_ADMIN_USERNAME; fi
if [[ -n $DG_ADMIN_PASSWORD ]]; then password=$DG_ADMIN_PASSWORD; fi
if [[ -n $DG_ADMIN_EMAIL ]]; then email=$DG_ADMIN_EMAIL; fi

    cat <<EOF | python manage.py shell
from django.contrib.auth import get_user_model

User = get_user_model()

if not User.objects.filter(username="$username").exists():
    User.objects.create_superuser("$username", "$email", "$password")
else:
    print('User "{}" exists already, not created'.format("$username"))
EOF
}

if [ "$1" = "start" ]
then
    start
    exit
fi
if  [ "$1" = "migrate" ]; then
    migrate
    exit
fi
if  [ "$1" = "init" ]; then
    migrate
    create_superuser
    load_initial_data
    exit
fi
if  [ "$1" = "mstart" ]; then
    migrate
    create_superuser
    if [[ $DG_LOAD_INITIAL_DATA && ($DG_LOAD_INITIAL_DATA == "true" || $DG_LOAD_INITIAL_DATA == "True") ]];  then
      load_initial_data
    fi
    start
    exit
fi

bash