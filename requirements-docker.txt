Django==3.2.9
psycopg2-binary==2.9.3
requests==2.26.0
whitenoise==5.3.0
sentry-sdk==1.4.3
PyJWT==2.3.0
keycloak==3.1.3
ddrr==3.1.0