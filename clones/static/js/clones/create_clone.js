$( document ).ready(function() {

        const formClone = $('#formCreateClone');

        function clearMessage(){
            let cellProgress = $('#cellProgress');
            cellProgress.removeClass('w-0');
            cellProgress.addClass('w-100');
            $('#lblMessage').prop('textContent', '');
        }

        function hideMessage(){
            $('#lblMessage').prop('textContent', '');
            $('#fgrMessage').addClass('invisible')
        }

        function addMessage(text){
            let cellProgress = $('#cellProgress');
            let lblMessage = $('#lblMessage');
            cellProgress.removeClass('w-100');
            cellProgress.addClass('w-0');
            cellProgress.addClass('invisible')
            lblMessage.prop('textContent', text);
            lblMessage.removeClass('invisible')
            $('#fgrMessage').removeClass('invisible')
        }

        function showProgress(withMessage = false){
            clearMessage();
            let cellProgress = $('#cellProgress');
            cellProgress.addClass('w-100');
            cellProgress.removeClass('invisible')
            $('#fgrMessage').removeClass('invisible')
        }

        formClone.on('shown.bs.modal', function () {
            clearMessage()
            let clone_id = $('#clone_id');
            clone_id.val('');
            $('#clone_db_username').val('');
            $('#clone_db_password').val('');
            let clone_protect = $('#clone_protect');
            clone_protect.prop('checked', false);
            log(clone_protect.prop('checked'));
            clone_id.focus();
        });

        formClone.on('hidden.bs.modal', function () {
            clearMessage();
            log('form HIDE');
            let modalForm = $('.modal-content');
            modalForm.css('background-color', '#FFF');
        });

        function getCloneStatus(clone_id) {
            return new Promise(function (resolve, reject) {
                log('Fetch STATUS')
                // Проверяем статус слона
                fetch('/clone/status?clone_id=' + clone_id, {
                    method: 'get',
                    headers: {
                        'X-CSRFToken': getCookie('csrftoken'),
                        'Content-Type': 'application/json;charset=utf-8',
                    },
                }).then(
                     function (response) {
                        log('Fetch STATUS. Response');
                        let message =  response.json()
                        message.then(function (data) {
                            const $res = {
                                code: response.status,
                                data: data,
                            };
                            log('Fetch STATUS. Response. ', $res);
                            resolve($res);
                        });
                    }
                ).catch( // Ошибка при запросе статуса клона
                    function (error) {
                        log('Fetch STATUS. Error');
                        const res = {
                            code: -1,
                            data: error.message,
                        }
                        log('Fetch STATUS. Error. ', res);
                        reject(res);
                    }
                );
            });
        }

        $('#btnCreateClone').on('click', async function () {
            // var token = getCookie('csrftoken');
            const clone_id = $('#clone_id').prop('value');
            let snapshot_id = ""
            const select = document.querySelector('#snapshot_id').getElementsByTagName('option')
            for (let i = 0; i < select.length; i++) {
                log("select[i].value", select[i].value)
                log("select[i].selected", select[i].selected)
                log("select[i]", select[i])
                if (select[i].selected === true) {
                    snapshot_id = select[i].value;
                    break
                }
            }
            const clone_db_username = $('#clone_db_username').prop('value')
            const clone_db_password = $('#clone_db_password').prop('value')
            const clone_protect = $('#clone_protect').prop('checked')

            let lblMessage = $('#lblMessage')
            let fgrMessage = $('#fgrMessage')
            let createCloneProgress = $('#createCloneProgress')

            // Отправить запрос на создание клона.
            // При успешном принятии запроса отдаётся код 201 и клон создаётся некоторое время.
            // Клон может быть успешно создан или во время создания может произойти ошибка.
            // Нужно через некоторое время проверить статус клона по его ID, чтобы точно узнать,
            // всё ли с ним в порядке и показать ответ пользователю

            let clone_info = {
                    clone_id: clone_id,
                    snapshot_id: snapshot_id,
                    clone_db_username: clone_db_username,
                    clone_db_password: clone_db_password,
                    clone_protect: clone_protect,
            };

            showProgress();
            log('Fetch CREATE')
            fetch('/clone/create', {
                method: 'post',
                headers: {
                    'X-CSRFToken': getCookie('csrftoken'),
                    'Content-Type': 'application/json;charset=utf-8',
                },
                body: JSON.stringify(clone_info)
            }).then(
                async function (response) {
                    log('Fetch CREATE. Response');
                    if (response.status === 201) {
                        // Запрос на создание клона принят.
                        // Нужно делать запросы статус клона для определения того,
                        // создался ли клон или произошла какая-то ошибка
                        log('Fetch CREATE. Response 201');
                        let clone_status = await getCloneStatus(clone_id);
                        log('Fetch CREATE. Response. Clone status 2: ', clone_status);
                        if (clone_status.code < 0) {
                            addMessage('Ошибка: ' + clone_status.data)
                            // lblMessage.prop('textContent', 'Ошибка: ' + clone_status.data)
                        } else {
                            log('Fetch CREATE. Response NOT 201');
                            if (clone_status.code >= 200 && clone_status.code < 300) {
                                log('Fetch CREATE. 200 <= Response < 300');
                                while (clone_status.data.data.status.code === 'CREATING') {
                                    log('Fetch CREATE. Response. Sleep');
                                    clone_status = await getCloneStatus(clone_id);
                                    await psleep(2000);
                                    log('Fetch CREATE. Response. GetStatus', clone_status);
                                }
                                // $createCloneProgress.addClass('invisible')
                                // $lblMessage.prop('textContent', '')
                                // $fgrMessage.addClass('invisible')
                                log('Fetch CREATE. Response. Reload document');
                                window.location.reload();
                            } else {
                                log('Fetch CREATE. Response OTHER');
                                addMessage(JSON.stringify(clone_status.data))
                                // lblMessage.prop('textContent', JSON.stringify(clone_status.data));
                                // createCloneProgress.addClass('invisible');
                            }
                        }

                    } else { // Что-то не так при первичном запросе создания нового клона
                        let message = response.json()
                        message.then(function (data) {
                            addMessage(JSON.stringify(data))
                            // lblMessage.prop('textContent', JSON.stringify(data))
                            // createCloneProgress.addClass('invisible')
                        });
                    }
                }
            ).catch( // Ошибка при первичном запросе создания нового клона
                function (err){
                    log('Fetch CREATE. Error: ', err);
                    addMessage(JSON.stringify(err))
                    // createCloneProgress.addClass('invisible')
                }
            );
        });

});