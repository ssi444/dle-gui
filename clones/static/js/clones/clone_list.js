$( document ).ready(function() {

    let modalConfirm = function(callback){
      $(".action_icon").on("click", function(event){
          $("#modalLblMessage").prop('textContent', event.target.dataset.msg)
          $("#modalLblAction").prop('textContent', event.target.dataset.action)
          $("#modalLblID").prop('textContent', event.target.id)
          $("#formConfirm").modal('show');
      });

      $(".clone_info").on("click", function(event){
          $("#input_connection_info_psql").val(event.target.dataset.connection_psql)
          $("#input_connection_info_jdbc").val(event.target.dataset.connection_jdbc)
          $("#formConnectionHint").modal('show');
      });

      $("#btn_connection_info_psql").on("click", function(){
        navigator.clipboard.writeText($("#input_connection_info_psql").prop('value'));
      });

      $("#btn_connection_info_jdbc").on("click", function(){
        navigator.clipboard.writeText($("#input_connection_info_jdbc").prop('value'));
      });

      $("#modalBtnConfirm").on("click", function(){
        callback(true);
        $("#formConfirm").modal('hide');
      });

      $("#modalBtnDismiss").on("click", function(){
        callback(false);
        $("#formConfirm").modal('hide');
      });
    };

    function cloneAction(action, cloneID,) {
        return new Promise(function (resolve, reject) {
                log('cloneAction')
                // Проверяем статус слона
                fetch('/clone/action?clone_id=' + cloneID + '&action=' + action, {
                    method: 'get',
                    headers: {
                        'X-CSRFToken': getCookie('csrftoken'),
                        'Content-Type': 'application/json;charset=utf-8',
                    },
                }).then(
                     function (response) {
                        log('cloneAction. Response');
                        let message =  response.json()
                        message.then(function (data) {
                            const $res = {
                                code: response.status,
                                data: data,
                            };
                            log('cloneAction. Response. ', $res);
                            resolve($res);
                        });
                    }
                ).catch( // Ошибка при запросе статуса клона
                    function (error) {
                        log('cloneAction. Error');
                        const res = {
                            code: -1,
                            data: error.message,
                        }
                        log('cloneAction. Error. ', res);
                        reject(res);
                    }
                );
        });
    }

    modalConfirm(async function(confirm){
      if(confirm){
          let cloneID = $("#modalLblID").prop('textContent')
          let action = $("#modalLblAction").prop('textContent')
          let result = await cloneAction(action, cloneID);
          if (result.code === 200) {
             window.location.reload();
          } else {
              let message = '';
              if (typeof result.data.data !== 'undefined') {
                  message = 'Код ('+result.code+'): '+ result.data.message + ' (' + result.data.data.message + ')'
              } else {
                  message = 'Код ('+result.code+'): '+ result.data.message
              }
              $("#modalLblErrorMessage").prop('textContent', message)
              $("#formError").modal('show');
          }

      }
    });

});