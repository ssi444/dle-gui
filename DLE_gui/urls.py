"""DLE_gui URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static
from . import views


urlpatterns = [
    path('i18n/', include('django.conf.urls.i18n')),
    path('', include('users.urls')),
    path('admin/', admin.site.urls),
    path('status/', include('status.urls')),
    path('clone/', include('clones.urls')),
    path('snapshot/', include('snapshots.urls')),
    path('accounts/', include('django.contrib.auth.urls')),
    path('help/', include('help.urls')),
    path('health', views.health_check),
    path('healthz', views.health_check),
] + static(settings.STATIC_ROOT, document_root=settings.STATIC_ROOT)
