$( document ).ready(function() {

    $(".language-dropdown-item").on("click", function(event){
        log('event: ', event)
        let language = event.target.dataset.language;
        // send post request
        log('SetLanguage: ', language)
        fetch('/i18n/setlang/', {
            method: 'POST',
            body: 'csrfmiddlewaretoken='+ getCookie('csrftoken') +'&language=' + language,
            headers: {
                'X-CSRFToken': getCookie('csrftoken'),
                'Content-Type': 'application/x-www-form-urlencoded',
                "credentials": "include"
            },
        }).then(
             function (response) {
                window.location.reload();
            }
        ).catch( // Ошибка при смене языкаs
            function (error) {
                log('SetLanguage. Error');
                const res = {
                    code: -1,
                    data: error.message,
                }
                log('SetLanguage. Error. ', res);
                reject(res);
            }
        );
    });




});