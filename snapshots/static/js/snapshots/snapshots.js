$( document ).ready(function() {

        $('.img_create').on('click', function (e) {
            var $modalForm = $('#formCreateClone');
            var $clone_id = $(this).prop('id');
            log($clone_id)

            const select = document.querySelector('#snapshot_id').getElementsByTagName('option')
            for (let i = 0; i < select.length; i++) {
                if (select[i].value === $clone_id) select[i].selected = true;
            }

            $modalForm.modal('show');
        });

});