# Database Lab Engine (DLE) GUI

Database Lab Engine (DLE) - открытый движок для создания тонких клонов БД.
На текущий момент работает с СУБД PostgreSQL. "Тонкое" клонирование работает за счёт 
снимков ZFS (основной бакэнд) или LVM. Позволяет очень быстро создать обособленную 
копию БД и работать с ней как с отдельной сущностью, не затрагивая основную БД.

Сайт - https://postgres.ai/

Бесплатно распространяется только основной движок. Графическое окружение, позволяющее
работать с DLE более удобным способом является платным и предоставляется как SaaS 
за отдельную плату (https://postgres.ai/pricing) 

Данное ПО является графической надстройкой над API DLE (https://gitlab.com/postgres-ai/database-lab/-/blob/master/api/swagger-spec/dblab_server_swagger.yaml)

> **⚠ DLE GUI не разрабатывается Postgres.ai и не является официальным UI для DLE. Это сторонняя надстройка**

## Лицензия
[Читать](./LICENSE.md)

## Возможности
 - Отображение статуса DLE и статистики по файловой системе
 - Отображение списка снашотов с возможностью быстро создать клон на основе любого снапшота
 - Отображение списка клонов с возможностью:
      - Снять/Установить признак "protected" (при protected=true клон не будет удалён, в случае если по нему 
        не было активности более заданного в файле конфигурации числа минут
      - Сбросить все изменения в клоне на начальное состояние
      - Сбросить клон на другой снимок с сохранением настроек
      - Удалить клон
 - Ограничение доступа к DLE GUI
 - Разграничение доступа на основе привилегий

SaaS от [postgres.ai](https://postgres.ai/) имеет ещё интеграцию с Joe Bot, историю и другие приятности. 
Тут этого нет. Как говорится: "Первая доза бесплатно" ;)

## Картинки

![status](./doc/snapshots.png)
![status](./doc/status.png)
![status](./doc/create-clone.png)
![status](./doc/reset_with_custom_snapshots.png)

## TODO
 - [ ] Логирование действий пользователей
      - [x] Создание клона
      - [x] Сброс клона  
      - [x] Установка/снятие защиты для клона
      - [x] Удаление клона
      - [ ] Создание снимка (после добавление данной возможности в API DLE)
 - [x] Привилегии
      - [x] Просмотр списка клонов, созданных этим пользователем
      - [x] Просмотр списка всех клонов  
      - [x] Создание нового клона
      - [x] Сброс клона, созданного этим пользователем
      - [x] Сброс любого клона  
      - [x] Установка/снятие защиты для клонов, созданных этим пользователем
      - [x] Установка/снятие защиты для любого клона
      - [x] Удаление клона, созданного этим пользователем
      - [x] Удаление любого клона
      - [x] Создание снапшота по требованию (после добавление данной возможности в API DLE)
 - [x] Роли для пользователей
      - [x] Администратор
        - [x] Просмотр списка всех клонов
        - [x] Создание нового клона
        - [x] Сброс любого клона
        - [x] Удаление любого клона
        - [x] Установка/снятие защиты для любого клона
        - [x] Создание снапшотов по требованию (после добавление данной возможности в API DLE)
      - [x] Оператор
        - [x] Просмотр списка всех клонов
        - [x] Создание нового клона
        - [x] Сброс любого клона
        - [x] Удаление клона, созданного этим пользователем
        - [x] Установка/снятие защиты для клонов, созданных этим пользователем
        - [x] Создание снапшотов по требованию (после добавление данной возможности в API DLE)
      - [x] Пользователь
         - [x] Просмотр списка клонов, созданных этим пользователем
         - [x] Создание нового клона
         - [x] Сброс клона, созданного этим пользователем
         - [x] Удаление клона, созданного этим пользователем
         - [x] Установка/снятие защиты для клонов, созданных этим пользователем
      - [x] Наблюдатель
         - [x] Просмотр списка всех клонов
 - [ ] Создание снапшота по требованию (после добавления такой возможности в API DLE. Сейчас снапшоты создаются только по расписанию, заданному в файле конфигурации)
 - [x] Авторизация через Keycloak
 - [x] Локализация (RU, EN)


## entrypoint.sh
Скрипт начального запуска содержит команды:
1. start - запуск DLE GUI
2. migrate - выполнение миграций
3. init - инициализация приложения (миграции, создание суперпользователя, загрузка начальных данных: лействий с клонами, привилегий, ролей, групп)
4. mstart - применение всех действий из init (загрузка начальных данных отдельно регулируется переменной $DG_LOAD_INITIAL_DATA) перед запуском приложения


## Запуск

По умолчанию переменная DG_LOAD_INITIAL_DATA установлена в значение `false`, чтобы при каждом запуске не грузить 
стартовые настройки. Поэтому при первом запуске нужно запустить DLE GUI с параметром `init`.

### Из исходного кода
1. Установить все необходимые зависимости
   ``` bash
   # pip install -r requirements.txt
   ```
2. Скопировать `.env-template` в `.env`, прописать нужные параметры и импортировать переменные `sourсe .env`
3. Запустить скрипт ./entrypoint.sh с параметрами:
   1. init
   2. mstart
   ``` bash
   $ python manage.py init
   $ python manage.py mstart
   ```

### С помощью Docker-контейнера
1. Сборка
   ``` bash
   $ docker build -t dlegui:v0.1.0 -f Dockerfile .
   ```
2. Запуск
   ``` bash
   $ cp .env-template .env
   $ echo "Прописываем в .env нужные значения"
   $ source .env
   $ docker run -it --rm --name dlegui -e DG_DATABASE_NAME=$DG_DATABASE_NAME -e DG_DATABASE_USER=$DG_DATABASE_USER -e DG_DATABASE_PASSWORD=$DG_DATABASE_PASSWORD -e DG_DATABASE_HOST=$DG_DATABASE_HOST -e DG_DATABASE_PORT=$DG_DATABASE_PORT -e DG_DBLAB_TOKEN=$DG_DBLAB_TOKEN -e DG_DBLAB_URL=$DG_DBLAB_URL -e DG_ADMIN_USERNAME=$DG_ADMIN_USERNAME -e DG_ADMIN_PASSWORD=$DG_ADMIN_PASSWORD -e DG_ADMIN_EMAIL=$DG_ADMIN_EMAIL -e DG_SECRET_KEY=$DG_SECRET_KEY -e DG_LOAD_INITIAL_DATA=$DG_LOAD_INITIAL_DATA -e DG_SENTRY_ENABLE=$DG_SENTRY_ENABLE -e DG_SENTRY_DSN=$DG_SENTRY_DSN -e DG_SENTRY_TRACES_SAMPLE_RATE=$DG_SENTRY_TRACES_SAMPLE_RATE -e SENTRY_ENVIRONMENT=$SENTRY_ENVIRONMENT -p 8000:8000 dlegui:v0.1.0 init
   $ docker run -it --rm --name dlegui -e DG_DATABASE_NAME=$DG_DATABASE_NAME -e DG_DATABASE_USER=$DG_DATABASE_USER -e DG_DATABASE_PASSWORD=$DG_DATABASE_PASSWORD -e DG_DATABASE_HOST=$DG_DATABASE_HOST -e DG_DATABASE_PORT=$DG_DATABASE_PORT -e DG_DBLAB_TOKEN=$DG_DBLAB_TOKEN -e DG_DBLAB_URL=$DG_DBLAB_URL -e DG_ADMIN_USERNAME=$DG_ADMIN_USERNAME -e DG_ADMIN_PASSWORD=$DG_ADMIN_PASSWORD -e DG_ADMIN_EMAIL=$DG_ADMIN_EMAIL -e DG_SECRET_KEY=$DG_SECRET_KEY -e DG_LOAD_INITIAL_DATA=$DG_LOAD_INITIAL_DATA -e DG_SENTRY_ENABLE=$DG_SENTRY_ENABLE -e DG_SENTRY_DSN=$DG_SENTRY_DSN -e DG_SENTRY_TRACES_SAMPLE_RATE=$DG_SENTRY_TRACES_SAMPLE_RATE -e SENTRY_ENVIRONMENT=$SENTRY_ENVIRONMENT -p 8000:8000 dlegui:v0.1.0
   ```
> **⚠ DLE GUI тестировался только с БД PostresSQL.**

При запуске контейнера выполняются следующие задачи:
 - Подключение к БД и запуск миграций
 - Создание суперпользователя, указанного в переменных окружения DG_ADMIN_*
   - по умолчанию DG_ADMIN_USERNAME=admin, DG_ADMIN_PASSWORD=admin, DG_ADMIN_EMAIL=admin@domain.local (прописано в entrypoint.sh)
   - если пользователь существует, то он не будет пересоздаваться или обновляться
     > **⚠ После первого запуска обязательно нужно сменить пароль суперпользователю**
  - Запуск сервера на порту 8000  

### С помощью Docker-compose
#### Внутренняя БД пользователей
```shell
cp .env-template .env-local-internal
```

Отредактировать файл .env-local-internal, подставив нужные значения. Особо:
1. DG_DBLAB_TOKEN
2. DG_DBLAB_URL

```shell
docker-compose -p dlegui --env-file .env-local-internal -f docker-compose-internal.yaml build
docker-compose -p dlegui --env-file .env-local-internal -f docker-compose-internal.yaml pull
docker-compose -p dlegui --env-file .env-local-internal -f docker-compose-internal.yaml up -d
```

#### Авторизация через Keycloak 
```shell
cp .env-template .env-local-keycloak
```

Отредактировать файл .env-local-keycloak, подставив нужные значения. Особо:
1. DG_AUTH_TYPE="keycloak"
2. DG_KEYCLOAK_ADDRESS="http://192.168.1.10:8081" - нужно указать ip-адрес системы, но не 127.0.0.1, т.к. 
   ПО работает в контейнере, а там 127.0.0.1 указывает на другую сущность, чем на хост-системе
3. DG_DBLAB_TOKEN
4. DG_DBLAB_URL

```shell
docker-compose -p dlegui --env-file .env-local-keycloak -f docker-compose-keycloak.yaml build
docker-compose -p dlegui --env-file .env-local-keycloak -f docker-compose-keycloak.yaml pull
docker-compose -p dlegui --env-file .env-local-keycloak -f docker-compose-keycloak.yaml up -d postgresql
docker-compose -p dlegui --env-file .env-local-keycloak -f docker-compose-keycloak.yaml up -d keycloak
```
Дождаться полной загрузки Keycloak. Появится строка (*Running the server in development mode. DO NOT use this configuration in production*)
```shell
docker-compose -p dlegui --env-file .env-local-keycloak -f docker-compose-keycloak.yaml logs -f keycloak
```

Скопировать содержимое файла `docker-compose-files/info.txt` в буфер обмена
```shell
docker exec -it dg-keycloak bash
```
Вставить скопированное содержимое, для создания настроек Keycloak. <br>
Заходим на адрес http://192.168.1.10:8081, авторизуемся под admin/admin, открывает Realm `dlegui`, Client `dlegui`, 
вкладка `Settings`. Меняем значение параметра `Fine Grain OpenID Connect Configuration -> User Info Signed Response Algorithm` 
на произвольное значение, сохраняем, возвращаем на `unsigned` и снова сохраняем.<br>
По умолчанию значение этого параметра вроде как `unsigned`, то при автоматизированном создании keycloak всё равно 
подписывает ответы для запросов userinfo, что приводит к ошибкам. Поэтому нужно принудительно указать `unsigned`. Если 
клиент создаётся вручную, то такой ошибки нет.
```shell
docker-compose -p dlegui --env-file .env-local-keycloak -f docker-compose-keycloak.yaml up -d dlegui
```

## Настройка приложения
[Читать](./doc/CONFIGURING.md)

## Sentry

Приложение имеет возможность интеграции с системой отслеживания ошибок [Sentry](https://sentry.io/).
Для включения функционала нужно задать переменные окружения:
 - DG_SENTRY_ENABLE - должно иметь одно из значений ('true', '1', 't') или ('false', '0', 'f')
 - DG_SENTRY_DSN - адрес приёмника (http://token@domain.name:9000/1)
 - DG_SENTRY_TRACES_SAMPLE_RATE - число от 0 и 1. [Подробнее](https://docs.sentry.io/platforms/python/guides/django/configuration/options/#traces-sample-rate)
 - SENTRY_ENVIRONMENT - название окружения. По умолчанию, если не задано, имеет значение "production"

## Для разработчиков
### Локализация
1. В начале шаблона прописать
     ``` jsregexp
   {% load i18n %}
   ```  
2. В тесте шаблона прописать
     ``` html
   <th scope="col">{% trans 'clone_count' %}</th>
   ```  

3. Войти в нужное "приложение"
    ``` bash
   $ cd clones
   ```
4. Пропарсить все шаблоны и занести в файл перевода .po все строки-шаблоны
    ``` bash
   $ django-admin makemessages -l ru
   ```
5. Открыть файл locale/ru/LC_MESSAGES/django.po, найти там добавленную 
   строку `msgid "clone_diff_size"` и в `msgstr` прописать перевод. 
   Если перед блоком есть комментарий вида `#, fuzzy`, то его нужно удалить
    ``` bash
    #: templates/clones/clone_list.html:45
    msgid "clone_diff_size"
    msgstr "Занимаемое место"
   ```
6. Скомпилировать тестовый файл перевода (.po) в бинарный (.mo)
    ``` bash
    $ django-admin compilemessages | grep "locale/ru"
   ```
