from django.shortcuts import render, redirect
from django.utils.translation import ugettext as _
from DLE_gui import settings
from users.views import check_auth


def show_help(request):
    if not check_auth(request):
        return redirect('/login')

    title = _('help_header')  # Помощь
    help_selected = 'active'
    version = settings.VERSION

    return render(request, 'index.html',
                  {"title": title,
                   "version": version,
                   "help_selected": help_selected,
                   "auth_type": settings.AUTH_TYPE})
