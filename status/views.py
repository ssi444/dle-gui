import json
import time

from django.shortcuts import render, redirect
import requests
from django.utils.translation import ugettext as _
import snapshots
import status
from DLE_gui import settings

from DLE_gui.settings import dlegui_logger
from users.models import ActionLog, Permissions, Group
from users.models import CloneActions
from users.views import user_has_permission, check_auth


def get_status(request, force_all: bool = False):
    headers = {'Verification-Token': settings.DBLAB['token']}
    r = requests.get(f"{settings.DBLAB['url']}/status", headers=headers)
    if r.status_code != 200:
        return None
    rjson = r.json()
    # permissions = set()
    # groups = Group.objects.filter(user=request.user)
    # print(f"Group count: {len(groups)}")
    # for group in groups:
    #     permissions = permissions.union(group.group.role.permission.all())
    # for group in request.user.groups.all():
    #     permissions = permissions.union(group.group.role.permission.all())
    visible_clones = []
    for clone in rjson["cloning"]["clones"]:
        # По умолчанию владелец всех клонов DLE, т.к. они могут создаваться через ci или dblab cli
        clone["owner"] = "DLE"
        # Если клон создавался через DLE GUI, то запись об этом должна быть в логе
        # Посмотрим для clone["id"] все логи с action=create и если что-то найдётся, то вытянем оттуда владельца
        log_items_create = ActionLog.objects.filter(clone=clone["id"], action=CloneActions.objects.get(action="create"))
        log_items_reset = ActionLog.objects.filter(clone=clone["id"], action=CloneActions.objects.get(action="reset"))
        log_items = log_items_create.union(log_items_reset)
        for item in log_items:
            if item.description == '':
                continue
            description = json.loads(item.description)
            if 'snapshot' in description and 'id' in description['snapshot']:
                if description['snapshot']['id'] == clone['snapshot']['id'] and \
                        description['createdAt'] == clone['createdAt']:
                    clone["owner"] = item.user.username
        if clone["owner"] == request.user.username or force_all or \
                user_has_permission(request.user, 'viewing_any_clones'):
                # Permissions.objects.get(permission='viewing_any_clones') in permissions:
            visible_clones.append(clone)
    rjson["cloning"]["clones"] = visible_clones
    return rjson


def show_status(request):
    if not check_auth(request):
        return redirect('/login')

    title = _('status_caption_header')  # "Статус"
    status_selected = 'active'
    version = settings.VERSION

    result = status.views.get_status(request)
    snapshots_list = snapshots.views.get_snapshots()
    if result is None or snapshots_list is None:
        return render(request, 'no_data.html',
                      {"title": title,
                       "status_selected": status_selected})
    clone_max_idle_minutes = 0
    for clone in result["cloning"]['clones']:
        clone_max_idle_minutes = clone['metadata']['maxIdleMinutes']
        break
    dlegui_logger.debug(f'{{\"clone_max_idle_minutes\": {clone_max_idle_minutes}, \"snapshots_list_count\": {len(snapshots_list)} }}')

    for pool in result["pools"]:
        fs_used_proc = round(pool['fileSystem']['used'] * 100 / pool['fileSystem']['size'])
        pool['fileSystem']['used_proc'] = fs_used_proc

    return render(request, 'index.html',
                  {"title": title,
                   "version": version,
                   "status_selected": status_selected,
                   "instance_data": result,
                   "fs_used_proc": fs_used_proc,
                   "snapshots_list": snapshots_list,
                   "clone_max_idle_minutes": clone_max_idle_minutes,
                   "auth_type": settings.AUTH_TYPE})
