from django.shortcuts import render, redirect
import requests
from DLE_gui import settings
from django.utils.translation import ugettext as _

from users.views import check_auth


def get_snapshots():
    headers = {'Verification-Token': settings.DBLAB['token']}
    r = requests.get(f"{settings.DBLAB['url']}/snapshots", headers=headers)
    if r.status_code != 200:
        return None
    return r.json()


def show_snapshots(request):
    if not check_auth(request):
        return redirect('/login')

    title = _('snapshots_caption_header')  # "Снимки"
    snapshot_selected = 'active'
    version = settings.VERSION

    result = get_snapshots()
    if result is None:
        return render(request, 'no_data.html',
                      {"title": title,
                       "snapshot_selected": snapshot_selected})

    return render(request, 'index.html',
                  {"title": title,
                   "version": version,
                   "snapshot_selected": snapshot_selected,
                   "snapshots_list": result,
                   "auth_type": settings.AUTH_TYPE})
