FROM python:3.9.6-slim-buster
WORKDIR /dlegui
COPY requirements-docker.txt ./requirements.txt
RUN pip install -r requirements.txt
COPY entrypoint.sh /
COPY manage.py ./
COPY clones ./clones
COPY DLE_gui ./DLE_gui
COPY locale ./locale
COPY snapshots ./snapshots
COPY static ./static
COPY status ./status
COPY help ./help
COPY templates ./templates
COPY users ./users
COPY initial-data ./initial-data
RUN python manage.py collectstatic
RUN sed -i 's/const DEBUG.*=.*/const DEBUG = false/' static/js/main.js
EXPOSE 8000
ENTRYPOINT ["/entrypoint.sh"]
CMD ["mstart"]