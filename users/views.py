import time

import jwt
import json
import random
import string
from django.contrib import auth
from django.contrib.auth.models import User
from django.shortcuts import render, redirect
from django.utils.translation import ugettext as _
from django.http import HttpRequest, HttpResponse
from django.contrib.auth import authenticate, login
from jwt import ExpiredSignatureError, InvalidSignatureError
from DLE_gui import settings
from DLE_gui.settings import dlegui_logger
from .models import Group, Permissions


def user_has_permission(user: User, permission: str):
    """
    Возвращает True, если пользователь имеет указанный доступ/разрешение
    """
    if user.is_superuser:
       return True
    permissions = set()
    user_groups = Group.objects.filter(user=user)
    for group in user_groups:
        permissions = permissions.union(group.group.role.permission.all())
    return Permissions.objects.get(permission=permission) in permissions


def inspect_keycloak_token(request: HttpRequest) -> bool:
    """
    Проверяет токен на актуальность.
    Если токен корректный и он не истёк, то возвращает True.
    Если токен истёк или некорретный, то удаляет данные токена
    из сессии, чтобы Middleware обновило его или заново провело аутентификацию,
    и возращает False
    """
    # Middlerawe авторизует пользователя и возвращает данные о нём и его токенах в сессии
    token_user_str = request.session["user"]
    tokens_str = request.session["tokens"]
    tokens = json.loads(tokens_str)
    key = '\n'.join(['-----BEGIN PUBLIC KEY-----', settings.KEYCLOAK_PUBLIC_KEY, '-----END PUBLIC KEY-----'])
    # Проверим, не закончился ли срок жизни токена. Если закончился, то
    # jwt.decode выдаёт ошибку ExpiredSignatureError
    # В этом случае вычищаем из сессии токены и отправляем на главную страницу,
    # чтобы повторно запустить процесс аутентификации
    try:
        jwt_decode = jwt.decode(tokens['access_token'], key, algorithms=['RS256'], audience=['dlegui', 'account'])
        dlegui_logger.debug(f'jwt_decode: {jwt_decode}')
        return True
    except ExpiredSignatureError:
        dlegui_logger.debug(f'Token expired!!!')
    except InvalidSignatureError:
        dlegui_logger.debug(f'Token invalid!!!')
    del (request.session["user"])
    del (request.session["tokens"])
    dlegui_logger.debug(f'token_user_str: {token_user_str}')
    dlegui_logger.debug(f'access_token: {tokens["access_token"]}')
    dlegui_logger.debug(f'refresh_token: {tokens["refresh_token"]}')
    return False


def check_auth(request: HttpRequest) -> bool:
    """
    Проверка, аутентифицирован ли пользователь.
    Если аутентификация производится через Keycloak, то
    проверяется также действителен ли токен. Если токен закончился, то он удаляется
    из сессии, чтобы Middleware обновило его или заново провело аутентификацию

    Если пользователь аутентифицирован и сессия активна, то возвращается True, иначе False
    """
    if settings.AUTH_TYPE == settings.AuthTypes.keycloak:
        if not inspect_keycloak_token(request):
            return False
        if not request.user.is_authenticated:
            return False
        return True
    else:
        if not request.user.is_authenticated or not request.user.is_active:
            return False
        else:
            return True


def auth(request: HttpRequest) -> HttpResponse:
    """
    Проверка, аутентифицирован ли пользователь.
    Если аутентификация производится через Keycloak, то
    проверяется также действителен ли токен, а также актуализируются
    пользовательские группы в соответствие с тем, что указано в токене
    """
    if settings.AUTH_TYPE == settings.AuthTypes.keycloak:
        # Middlerawe авторизует пользователя и возвращает данные о нём и его токенах в сессии
        if not inspect_keycloak_token(request):
            return redirect('/login')
        token_user_str = request.session["user"]
        token_user = json.loads(token_user_str)
        tokens_str = request.session["tokens"]
        tokens = json.loads(tokens_str)

        # Если пользователь аутентифицирован, то посмотрим, есть ли он во внутренней БД пользователей Django.
        # Если пользователь есть, то делаем ему login, чтобы везде логика проверок прав оставалась такой же,
        # как при работе с внутренней БД пользователей.
        try:
            current_user = User.objects.get(username=token_user['preferred_username'])
            login(request, current_user)
        except User.DoesNotExist:  # Если пользователя нет, то создаём его
            current_user = User.objects.create_user(token_user['preferred_username'],
                                                    password=''.join(random.choices(string.ascii_lowercase +
                                                                                    string.ascii_uppercase +
                                                                                    string.digits, k=20)))
            current_user.save()
        # except User.MultipleObjectsReturned:
        #     print("Найдено несколько пользователей!")

        # Каждый раз после логина пользователя нужно проверить, не изменился ли у него список групп в Keycloak.
        # Следует привести список групп пользователя в соответствие с тем, что у него назначено в кейклоке.
        # Если назначена группа Admins, то помимо включения пользователяв соответствующую группу, нужно сделать
        # его ещё и суперпользователем, чтобы он мог пользоваться админкой Django
        user_groups_from_token = token_user.get(settings.DG_KEYCLOAK_GROUPS_KEY_NAME, [])
        # Если у пользователя назначена группа Admins, но он не является супервользователем,
        # то сделаем его таковым
        if 'admins' in list(map(lambda g: g.lower(), user_groups_from_token)) and not current_user.is_superuser:
            current_user.is_staff = True
            current_user.is_admin = True
            current_user.is_superuser = True
            current_user.save()
        # иначе уберём у пользователя статус суперпользователя
        elif current_user.is_superuser \
                and 'admins' not in list(map(lambda g: g.lower(), user_groups_from_token)):
            current_user.is_staff = False
            current_user.is_admin = False
            current_user.is_superuser = False
            current_user.save()

        user_groups = Group.objects.filter(user=current_user)
        all_groups = Group.objects.all()
        # Просмотрим список всех групп Django.
        # Если группа есть в токене и пользователь в ней не состоит, то добавим пользователя в такую группу
        for a_g in all_groups:
            if a_g.name.lower() in list(map(lambda g: g.lower(), user_groups_from_token)) and a_g not in user_groups:
                a_g.user.add(current_user)
                # current_user.groups.add(a_g)

        # Обновим список групп пользователя
        user_groups = Group.objects.filter(user=current_user)
        # Просмотрим список всех групп пользователя.
        # Если пользователь состоит в группе, а её нет в токене, то удалим пользователя из этой группы
        for u_g in user_groups:
            if u_g.name.lower() not in list(map(lambda g: g.lower(), user_groups_from_token)):
                u_g.user.remove(current_user)

        dlegui_logger.debug(f'token_user_str: {token_user_str}')
        dlegui_logger.debug(f'access_token: {tokens["access_token"]}')
        dlegui_logger.debug(f'refresh_token: {tokens["refresh_token"]}')

        return redirect('/status')
    else:
        if request.user.is_authenticated and request.user.is_active:
            if request.path_info in ('/', '/login'):
                return redirect('/status')
        return user_login(request)


def show_auth_page(request: HttpRequest, is_error: bool = False) -> HttpResponse:
    """
    Отображение формы ввода имени пользователя и пароля.
    Параметр is_error определяет, нужно ли отображать на
    форме надпись о том, что имя пользователя или пароль
    были введены неверно
    """
    return render(request, 'users/login.html',
                  {"title": _('authentication'), "is_error": is_error})


def user_login(request: HttpRequest) -> HttpResponse:
    """
    Аутентификация пользователя по внутренней БД пользователей.
    Ожидается POST-запрос, в котором должны быть указаны
    параметры user_login и user_password
    Если обращение было не POST-запросом, то происходит перенаправление
    на страницу ввода логина и пароля
    """
    if request.method == 'GET' or 'user_login' not in request.POST or 'user_password' not in request.POST:
        return show_auth_page(request, False)
    username = request.POST.get('user_login')
    password = request.POST.get('user_password')
    if len(str(username).strip()) < 2 or len(str(password).strip()) < 1:
        return show_auth_page(request, True)
    user = authenticate(username=username, password=password)
    if user is not None and user.is_active:  # Правильный пароль и пользователь не заблокирован
        # auth.login(request, user)
        login(request, user)
        # Перенаправление на страницу статуса
        return redirect('/status')
    else:  # Отображение страницы с ошибкой
        return show_auth_page(request, True)


def user_logout(request: HttpRequest) -> HttpResponse:
    if settings.AUTH_TYPE == settings.AuthTypes.keycloak:
        return redirect("/login")
        # return redirect("/kc/logout")
        # auth.logout(request)
    else:
        auth.logout(request)
        return redirect("/login")

