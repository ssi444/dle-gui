from keycloak.extensions.django import AuthenticationMiddleware as AuthenticationMiddleware
from django.http import HttpRequest, HttpResponse
from urllib.parse import urlparse

from DLE_gui.settings import dlegui_logger


class AuthenticationMiddleware(AuthenticationMiddleware):
    def __call__(self, request: HttpRequest) -> HttpResponse:

        # health request
        if request.path in ['/health', '/healthz']:
            return self.get_response(request)

        # callback request
        dlegui_logger.debug(f'request.build_absolute_uri(request.path): {request.build_absolute_uri(request.path)}')
        # if request.build_absolute_uri(request.path) == self.callback_url:
        if request.path == urlparse(self.callback_url).path:
            return self.callback(request)

        # logout request
        elif request.path == self.logout_uri:
            return self.logout(request)

        # logout redirect uri
        elif request.path == self.logout_redirect_uri:
            return self.get_response(request)

        # unauthorized request
        elif "user" not in request.session:
            return self.login(request)

        # authorized request
        else:
            return self.get_response(request)
