$( document ).ready(function() {

      $(".clone_reset").on("click", function(event){
          $("#reset_clone_id").val(event.target.id)
          log("test1", event.target.id)
          $("#formResetClone").modal('show');
      });


        const formResetClone = $('#formResetClone');

        function clearResetMessage(){
            let cellProgress = $('#cellResetProgress');
            cellProgress.removeClass('w-0');
            cellProgress.addClass('w-100');
            $('#lblResetMessage').prop('textContent', '');
        }

        function hideResetMessage(){
            $('#lblResetMessage').prop('textContent', '');
            $('#fgrResetMessage').addClass('invisible')
        }

        function addResetMessage(text){
            let cellProgress = $('#cellResetProgress');
            let lblMessage = $('#lblResetMessage');
            cellProgress.removeClass('w-100');
            cellProgress.addClass('w-0');
            cellProgress.addClass('invisible')
            lblMessage.prop('textContent', text);
            lblMessage.removeClass('invisible')
            $('#fgrResetMessage').removeClass('invisible')
        }

        function showResetProgress(withMessage = false){
            clearResetMessage();
            let cellProgress = $('#cellResetProgress');
            cellProgress.addClass('w-100');
            cellProgress.removeClass('invisible')
            $('#fgrResetMessage').removeClass('invisible')
        }

        formResetClone.on('shown.bs.modal', function (event) {
            clearResetMessage()
            let snapshot_id = $('#reset_snapshot_id');
            snapshot_id.focus();
        });

        formResetClone.on('hidden.bs.modal', function () {
            clearResetMessage();
            log('form HIDE');
            let modalForm = $('.modal-content');
            modalForm.css('background-color', '#FFF');
        });

       function getCloneStatus(clone_id) {
            return new Promise(function (resolve, reject) {
                log('Fetch STATUS')
                // Проверяем статус слона
                fetch('/clone/status?clone_id=' + clone_id, {
                    method: 'get',
                    headers: {
                        'X-CSRFToken': getCookie('csrftoken'),
                        'Content-Type': 'application/json;charset=utf-8',
                    },
                }).then(
                     function (response) {
                        log('Fetch STATUS. Response');
                        let message =  response.json()
                        message.then(function (data) {
                            const $res = {
                                code: response.status,
                                data: data,
                            };
                            log('Fetch STATUS. Response. ', $res);
                            resolve($res);
                        });
                    }
                ).catch( // Ошибка при запросе статуса клона
                    function (error) {
                        log('Fetch STATUS. Error');
                        const res = {
                            code: -1,
                            data: error.message,
                        }
                        log('Fetch STATUS. Error. ', res);
                        reject(res);
                    }
                );
            });
        }

        $('#btnResetClone').on('click', async function () {
            // var token = getCookie('csrftoken');
            const clone_id = $('#reset_clone_id').prop('value');
            let snapshot_id = ""
            const select = document.querySelector('#reset_snapshot_id').getElementsByTagName('option')
            for (let i = 0; i < select.length; i++) {
                log("select[i].value", select[i].value)
                log("select[i].selected", select[i].selected)
                log("select[i]", select[i])
                if (select[i].selected === true) {
                    snapshot_id = select[i].value;
                    break
                }
            }
            let lblMessage = $('#lblMessage')
            let fgrMessage = $('#fgrMessage')
            let resetCloneProgress = $('#resetCloneProgress')

            // Отправить запрос на сброс клона.
            // При успешном принятии запроса отдаётся код 200 и клон пересоздаётся некоторое время.
            // Клон может быть успешно пересоздан или во время пересоздания может произойти ошибка.
            // Нужно через некоторое время проверить статус клона по его ID, чтобы точно узнать,
            // всё ли с ним в порядке и показать ответ пользователю

            let clone_info = {
                    clone_id: clone_id,
                    snapshot_id: snapshot_id,
            };

            showResetProgress();
            log('Fetch RESET')
            fetch('/clone/reset', {
                method: 'post',
                headers: {
                    'X-CSRFToken': getCookie('csrftoken'),
                    'Content-Type': 'application/json;charset=utf-8',
                },
                body: JSON.stringify(clone_info)
            }).then(
                async function (response) {
                    log('Fetch RESET. Response');
                    if (response.status === 200) {
                        // Запрос на сброс клона принят.
                        // Нужно делать запросы статус клона для определения того,
                        // сбросился ли клон или произошла какая-то ошибка
                        log('Fetch RESET. Response 200');
                        let clone_status = await getCloneStatus(clone_id);
                        log('Fetch RESET. Response. Clone status 2: ', clone_status);
                        if (clone_status.code < 0) {
                            addResetMessage('Ошибка: ' + clone_status.data)
                        } else {
                            log('Fetch RESET. Response NOT 201');
                            if (clone_status.code >= 200 && clone_status.code < 300) {
                                log('Fetch RESET. 200 <= Response < 300');
                                while (clone_status.data.data.status.code === 'RESETTING') {
                                    log('Fetch RESET. Response. Sleep');
                                    clone_status = await getCloneStatus(clone_id);
                                    await psleep(2000);
                                    log('Fetch RESET. Response. GetStatus', clone_status);
                                }
                                log('Fetch RESET. Response. Reload document');
                                window.location.reload();
                            } else {
                                log('Fetch RESET. Response OTHER');
                                addResetMessage(JSON.stringify(clone_status.data))
                            }
                        }

                    } else { // Что-то не так при первичном запросе сброса клона
                        let message = response.json()
                        message.then(function (data) {
                            addResetMessage(JSON.stringify(data))
                        });
                    }
                }
            ).catch( // Ошибка при первичном запросе сброса клона
                function (err){
                    log('Fetch RESET. Error: ', err);
                    addResetMessage(JSON.stringify(err))
                }
            );
        });

});