import time

from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.utils.translation import ugettext as _
import requests, re, json

import status
import snapshots
from DLE_gui import settings
from DLE_gui.settings import dlegui_logger
from users.models import CloneActions
from users.models import ActionLog
from users.views import user_has_permission, check_auth


def show_clones(request):
    if not check_auth(request):
        return redirect('/login')

    title = _('clone_list_header')  # Клоны
    clone_selected = 'active'
    version = settings.VERSION

    result = status.views.get_status(request)
    snapshots_list = snapshots.views.get_snapshots()
    if result is None or snapshots_list is None:
        return render(request, 'no_data.html',
                      {"title": title,
                       "clone_selected": clone_selected})
    clone_max_idle_minutes = 0
    for clone in result["cloning"]['clones']:
        clone_max_idle_minutes = clone['metadata']['maxIdleMinutes']
        break
    dlegui_logger.debug(f'{{\"clone_max_idle_minutes\": {clone_max_idle_minutes}, \"snapshots_list_count\": {len(snapshots_list)} }}')
    return render(request, 'index.html',
                  {"title": title,
                   "version": version,
                   "clone_selected": clone_selected,
                   "instance_data": result,
                   "snapshots_list": snapshots_list,
                   "clone_max_idle_minutes": clone_max_idle_minutes,
                   "auth_type": settings.AUTH_TYPE})


def dblab_clone_create(clone_id: str, snapshot_id: str, protected: bool, db_username: str, db_password: str):
    headers = {'Verification-Token': settings.DBLAB['token']}
    data = {"id": clone_id,
            "snapshot": {
                "id": snapshot_id
            },
            "protected": protected,
            "db": {
                "username": db_username,
                "password": db_password,
                "restricted": False
            }
            }
    r = requests.post(f"{settings.DBLAB['url']}/clone", headers=headers, data=json.dumps(data))
    return r


def dblab_clone_protected(clone_id: str, protected: bool):
    headers = {'Verification-Token': settings.DBLAB['token']}
    data = {"protected": protected}
    r = requests.patch(f"{settings.DBLAB['url']}/clone/{clone_id}", headers=headers, data=json.dumps(data))
    return r


def dblab_clone_reset(clone_id: str):
    headers = {'Verification-Token': settings.DBLAB['token']}
    r = requests.post(f"{settings.DBLAB['url']}/clone/{clone_id}/reset", headers=headers)
    return r


def dblab_clone_delete(clone_id: str):
    headers = {'Verification-Token': settings.DBLAB['token']}
    r = requests.delete(f"{settings.DBLAB['url']}/clone/{clone_id}", headers=headers)
    return r


def dblab_clone_status(clone_id: str):
    headers = {'Verification-Token': settings.DBLAB['token']}
    r = requests.get(f"{settings.DBLAB['url']}/clone/{clone_id}", headers=headers)
    return r


def get_response(code: int, message: str, data: str = ''):
    if code not in (200, 201, 202):
        code_text = "ERROR"
    else:
        code_text = "OK"
    if len(data) > 0:
        content = f'{{"code": "{code_text}", "message": "{message}", "data": {data} }}'
    else:
        content = f'{{"code": "{code_text}", "message": "{message}"}}'
    response = HttpResponse(content,
                            content_type='application/json')
    response.status_code = code
    return response


def clone_owner(clone_id: str):
    clone_info = dblab_clone_status(clone_id).json()
    log_items_create = ActionLog.objects.filter(clone=clone_id, action=CloneActions.objects.get(action="create"))
    log_items_reset = ActionLog.objects.filter(clone=clone_id, action=CloneActions.objects.get(action="reset"))
    log_items = log_items_create.union(log_items_reset)
    for item in log_items:
        if item.description == '':
            continue
        description = json.loads(item.description)
        if description['snapshot']['id'] == clone_info['snapshot']['id'] and \
                description['createdAt'] == clone_info['createdAt']:
            return item.user.username
    return "DLE"


def clone_create(request):
    """
        Функция создания клона.
        На вход должны быть переданы следующие параметры:
            1. Имя(id) клона (header: clone_id)
            2. Имя(id) снимка (header: snapshot_id)
            3. Пользователь БД, который в клоне будет создан как суперадмин  (header: clone_db_username)
            4. Пароль пользователя БД (header: clone_db_password)
            5. Признак защищённости  (header: clone_protect)
               (при отсутствии активности в клоне более заданного в настройках периода,
               клоны, не имеющие такого признака, удаляются)
    """
    if not request.user.is_authenticated or not request.user.is_active:
        return redirect('/login')
    if not user_has_permission(request.user, 'creating_new_clone'):
        return get_response(403, _('error_403_no_rights_on_clone_create'))  # "Нет прав на создание клонов!"
    if request.method != 'POST':
        return get_response(400, _('error_403_create_clone_only_via_post_request'))  # "Создание клона разрешено только через POST запрос"
    try:
        # body = request.POST
        body = json.loads(request.body.decode("utf-8"))
    except Exception:
        return get_response(400, _('error_400_must_pass_clone_parameters_in_body'))  # "Некорректный запрос. Параметры клона должны быть переданы в BODY запроса"
    if 'clone_id' not in body or len(body['clone_id']) < 2:
        return get_response(400, _('error_400_clone_id_must_be_no_less_than_two_symbols'))  # "Идентификатор клона должен быть не менее 2-х символов"
    if 'clone_db_username' not in body or len(body['clone_db_username']) < 2 or \
            'clone_db_password' not in body or len(body['clone_db_password']) < 2:
        return get_response(400, _('error_400_username_and_pass_must_be_no_less_than_two_symbols'))  # "Имя пользователя БД и пароль должны быть не менее 2-х символов"
    if 'snapshot_id' not in body or len(body['snapshot_id']) < 1:
        return get_response(400, _('error_400_missing_shapshot_id'))  # "Отсутствует идентификатор снимка"
    re_allowed_symbols = re.compile('^[-a-zA-Z0-9_@/]+$')
    if not bool(re_allowed_symbols.match(body['snapshot_id'])):
        return get_response(400, _('error_400_invalid_symbols_in_snapshot_id'))  # "Идентификатор снимка содержит неразрешённые символы"
    if not bool(re_allowed_symbols.match(body['clone_id'])):
        return get_response(400, _('error_400_invalid_symbols_in_clone_id'))  # "Идентификатор клона содержит неразрешённые символы"
    dle_status = status.views.get_status(request, True)
    list1 = filter(lambda item: item['id'] == body['clone_id'], dle_status["cloning"]['clones'])
    if len(list(list1)) > 0:
        return get_response(406,  _('error_406_clone_already_exist'))  # "Клон с таким идентификатором уже существует"

    if str(body['clone_protect']).lower() == "true":
        clone_protect = True
    else:
        clone_protect = False
    dblab_result = dblab_clone_create(body['clone_id'], body['snapshot_id'], clone_protect,
                                      body['clone_db_username'], body['clone_db_password'])
    j = dblab_result.json()
    action = CloneActions.objects.get(action="create")
    if dblab_result.status_code != 201:
        alog = ActionLog(user=request.user, action=action, success=False,
                         clone=body['clone_id'], description=json.dumps(j))
        alog.save()
        return get_response(dblab_result.status_code, message=_('error_creating_of_the_clone'), data=json.dumps(j))  # "Ошибка создания клона."
    else:
        alog = ActionLog(user=request.user, action=action, success=True,
                         clone=body['clone_id'], description=json.dumps(j))
        alog.save()
        return get_response(dblab_result.status_code, message=_('clone_is_being_created'), data=json.dumps(j))  # "Клон создаётся"


def dblab_clone_reset_with_new_snapshot(clone_id: str, snapshot_id: str):
    headers = {'Verification-Token': settings.DBLAB['token']}
    data = {"id": clone_id,
            "snapshotID": snapshot_id
            }
    r = requests.post(f"{settings.DBLAB['url']}/clone/{clone_id}/reset", headers=headers, data=json.dumps(data))
    return r


def clone_reset_with_new_snapshot(request):
    """
        Функция сброса клона.
        На вход должны быть переданы следующие параметры:
            1. Имя(id) клона (header: clone_id)
            2. Имя(id) снимка (header: snapshot_id)
    """
    if not request.user.is_authenticated or not request.user.is_active:
        return redirect('/login')
    if request.method != 'POST':
        return get_response(400, _('error_403_create_clone_only_via_post_request'))  # "Создание клона разрешено только через POST запрос"
    try:
        body = json.loads(request.body.decode("utf-8"))
    except Exception:
        return get_response(400, _('error_400_incorrect_request_clone_parameters_missing_in_the_body'))  # "Некорректный запрос. Параметры клона должны быть переданы в BODY запроса"
    if 'clone_id' not in body or len(body['clone_id']) < 2:
        return get_response(400, _('error_400_clone_id_must_be_no_less_than_two_symbols'))  # "Идентификатор клона должен быть не менее 2-х символов"
    if not user_has_permission(request.user, 'resetting_any_clones'):
        if not (clone_owner(body['clone_id']) == request.user.username \
                and user_has_permission(request.user, 'resetting_your_own_clones')):
            msg = _('error_403_you_have_no_rights_on_resetting_clone') #  Нет прав на сброс клона
            return get_response(403, f"{ msg } <{body['clone_id']}>!")
    if 'snapshot_id' not in body or len(body['snapshot_id']) < 1:
        return get_response(400, _('error_400_missing_shapshot_id'))  # "Отсутствует идентификатор снимка"
    re_allowed_symbols = re.compile('^[-a-zA-Z0-9_@/]+$')
    if not bool(re_allowed_symbols.match(body['snapshot_id'])):
        return get_response(400, _('error_400_invalid_symbols_in_snapshot_id'))  # "Идентификатор снимка содержит неразрешённые символы"
    if not bool(re_allowed_symbols.match(body['clone_id'])):
        return get_response(400, _('error_400_invalid_symbols_in_clone_id'))  # "Идентификатор клона содержит неразрешённые символы"

    dblab_result = dblab_clone_reset_with_new_snapshot(body['clone_id'], body['snapshot_id'])
    action = CloneActions.objects.get(action="reset")
    if dblab_result.status_code != 200:
        j = dblab_result.json()
        alog = ActionLog(user=request.user, action=action, success=False,
                         clone=body['clone_id'], description=json.dumps(j))
        alog.save()
        return get_response(dblab_result.status_code, message=_('clone_reset_error'), data=json.dumps(j))  # "Ошибка сброса клона."
    else:
        time.sleep(6)
        dblab_result = dblab_clone_status(body['clone_id'])
        j = dblab_result.json()
        alog = ActionLog(user=request.user, action=action, success=True,
                         clone=body['clone_id'], description=json.dumps(j))
        alog.save()
        return get_response(dblab_result.status_code, message=_('clone_is_being_created'), data=json.dumps(j))  # "Клон создаётся"


def clone_status(request):
    if not request.user.is_authenticated or not request.user.is_active:
        return redirect('/login')
    try:
        body = request.GET
    except Exception:
        return get_response(400, _('error_400_must_pass_clone_parameters_in_body'))  # "Некорректный запрос. Параметры клона должны быть переданы в BODY запроса"

    if 'clone_id' not in body or len(body['clone_id']) < 2:
        return get_response(400, _('error_400_clone_id_must_be_no_less_than_two_symbols'))  # "Идентификатор клона должен быть не менее 2-х символов"
    re_allowed_symbols = re.compile('^[-a-zA-Z0-9_@/]+$')
    if not bool(re_allowed_symbols.match(body['clone_id'])):
        return get_response(400, _('error_400_invalid_symbols_in_clone_id'))  # "Идентификатор клона содержит неразрешённые символы"
    dblab_result = dblab_clone_status(body['clone_id'])
    j = dblab_result.json()
    dlegui_logger.debug(f'Result json: {json.dumps(j)}')
    if dblab_result.status_code != 200:
        return get_response(dblab_result.status_code, message=_('error_getting_clone_status'), data=json.dumps(j))  # "Ошибка получения статуса клона"
    else:
        if j['status']['code'] == 'CREATING':
            return get_response(dblab_result.status_code, message=_('clone_is_being_created'), data=json.dumps(j))  # "Клон создаётся"
        else:
            return get_response(dblab_result.status_code, message=_('clone_has_been_created'), data=json.dumps(j))  # "Клон создан"


def clone_action(request):
    if not request.user.is_authenticated or not request.user.is_active:
        return redirect('/login')
    try:
        body = request.GET
    except Exception:
        return get_response(400, _('error_400_must_pass_clone_parameters_in_body'))  # "Некорректный запрос. Параметры клона должны быть переданы в BODY запроса"



    if 'clone_id' not in body or len(body['clone_id']) < 2:
        return get_response(400,  _('error_400_clone_id_must_be_no_less_than_two_symbols'))  # "Идентификатор клона должен быть не менее 2-х символов"
    re_allowed_symbols = re.compile('^[-a-zA-Z0-9_@/]+$')
    if not bool(re_allowed_symbols.match(body['clone_id'])):
        return get_response(400, _('error_400_invalid_symbols_in_clone_id'))  # "Идентификатор клона содержит неразрешённые символы"
    if 'action' not in body or body['action'] not in ['protect', 'unprotect', 'reset', 'destroy']:
        return get_response(400, _('error_400_this_action_cannot_be_completed'))  # "Указанное действие не может быть выполнено!"

    action = CloneActions.objects.get(action=body['action'])

    if body['action'] == 'protect':
        if not user_has_permission(request.user, 'protecting_any_clones'):
            if not (clone_owner(body['clone_id']) == request.user.username
                    and user_has_permission(request.user, 'protecting_your_own_clones')):
                msg = _('error_403_you_have_no_rights_on_setting_clone_protection')  # "Нет прав на установку защиты для клона
                return get_response(403, f"{ msg } <{body['clone_id']}>!")
        dblab_result = dblab_clone_protected(body['clone_id'], True)
        j = dblab_result.json()
        dlegui_logger.debug(f'Result json: {json.dumps(j)}')
        if dblab_result.status_code != 200:
            alog = ActionLog(user=request.user, action=action, success=False,
                             clone=body['clone_id'], description=json.dumps(j))
            alog.save()
            return get_response(dblab_result.status_code, message=_('error_on_setting_clone_protection'), # "Ошибка установки признака protected для клона"
                                data=json.dumps(j))
        else:
            alog = ActionLog(user=request.user, action=action, success=True, clone=body['clone_id'])
            alog.save()
            return get_response(dblab_result.status_code, message=_('clone_settings_have_been_refreshed'), data=json.dumps(j))  # "Клон обновлён"
    elif body['action'] == 'unprotect':
        if not user_has_permission(request.user, 'protecting_any_clones'):
            if not (clone_owner(body['clone_id']) == request.user.username
                        and user_has_permission(request.user, 'protecting_your_own_clones')):
                msg = _('error_403_you_have_no_rights_on_removing_clone_protection')  # "Нет прав на снятие защиты для клона"
                return get_response(403, f"{ msg } <{body['clone_id']}>!")
        dblab_result = dblab_clone_protected(body['clone_id'], False)
        j = dblab_result.json()
        dlegui_logger.debug(f'Result json: {json.dumps(j)}')
        if dblab_result.status_code != 200:
            alog = ActionLog(user=request.user, action=action, success=False,
                             clone=body['clone_id'], description=json.dumps(j))
            alog.save()
            return get_response(dblab_result.status_code, message=_('error_on_removing_clone_protection'),  # "Ошибка снятия признака protected для клона"
                                data=json.dumps(j))
        else:
            alog = ActionLog(user=request.user, action=action, success=True, clone=body['clone_id'])
            alog.save()
            return get_response(dblab_result.status_code, message=_('clone_settings_have_been_refreshed'), data=json.dumps(j))  # "Клон обновлён"
    elif body['action'] == 'reset':
        if not user_has_permission(request.user, 'resetting_any_clones'):
            if not (clone_owner(body['clone_id']) == request.user.username \
                        and user_has_permission(request.user, 'resetting_your_own_clones')):
                msg =  _('error_403_you_have_no_rights_on_resetting_clone')  # "Нет прав на сброс клона
                return get_response(403, f"{ msg } <{body['clone_id']}>!")
        dblab_result = dblab_clone_reset(body['clone_id'])
        if dblab_result.status_code != 200:
            j = dblab_result.json()
            dlegui_logger.debug(f'Result json: {json.dumps(j)}')
            alog = ActionLog(user=request.user, action=action, success=False,
                             clone=body['clone_id'], description=json.dumps(j))
            alog.save()
            return get_response(dblab_result.status_code, message=_('error_on_resetting_clone'),  # "Ошибка сброса изменений для клона"
                                data=json.dumps(j))
        else:
            alog = ActionLog(user=request.user, action=action, success=True, clone=body['clone_id'])
            alog.save()
            return get_response(dblab_result.status_code, message=_('clone_has_been_reset'))  # "Клон сброшен"
    elif body['action'] == 'destroy':
        if not user_has_permission(request.user, 'deleting_any_clones'):
            if not (clone_owner(body['clone_id']) == request.user.username \
                        and user_has_permission(request.user, 'deleting_your_own_clones')):
                msg = _('error_403_you_have_no_rights_on_deleting_clone')  # "Нет прав на удаление клона
                return get_response(403, f"{ msg } <{body['clone_id']}>!")
        dblab_result = dblab_clone_delete(body['clone_id'])
        if dblab_result.status_code != 200:
            j = dblab_result.json()
            dlegui_logger.debug(f'Result json: {json.dumps(j)}')
            alog = ActionLog(user=request.user, action=action, success=False,
                             clone=body['clone_id'], description=json.dumps(j))
            alog.save()
            return get_response(dblab_result.status_code, message=_('error_on_deleting_clone'),  # "Ошибка удаления клона"
                                data=json.dumps(j))
        else:
            alog = ActionLog(user=request.user, action=action, success=True, clone=body['clone_id'])
            alog.save()
            return get_response(dblab_result.status_code, message=_('clone_has_been_deleted'))  # "Клон удалён"
