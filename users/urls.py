from django.urls import path
from . import views
from django.conf.urls import url

urlpatterns = [
    path('', views.auth),
    path('login', views.auth),
    path('logout', views.user_logout),
]